<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Post extends Model
{
    use HasFactory;
    protected $fillable =[
      'header',
      'content'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
