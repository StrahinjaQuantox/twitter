<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Post;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //$posts = \App\Models\Post::all();

         $posts = Cache::rememberForever('posts', function(){
             return Post::with('user')->get();
         });


        //dd(Auth()->user()->id);
        return view('home',compact('posts'));
    }


}
