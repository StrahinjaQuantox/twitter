<?php

namespace App\Http\Requests\Post;


use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !\Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header' => 'required',
            'content' => 'required',
            
        ];
    }
    public function messages() {
       return [
           'header.required' => 'Header is required',
           'content.required' => 'Content is required'
       ];
    }
}
