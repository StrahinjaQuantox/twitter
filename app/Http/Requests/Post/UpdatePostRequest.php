<?php
namespace App\Http\Requests\Post;
use Illuminate\Foundation\Http\FormRequest;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdatePostRequest
 *
 * @author quantox
 */
class UpdatePostRequest extends FormRequest {
    public function authorize()
    {
        return !\Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header' => 'required',
            'content' => 'required',
            
        ];
    }
    public function messages() {
       return [
           'header.required' => 'Header is required',
           'content.required' => 'Content is required'
       ];
    }
}
