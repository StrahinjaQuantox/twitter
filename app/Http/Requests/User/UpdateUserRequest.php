<?php 
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest{
   
    public function authorize()
    {
        return !\Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        
        return [
            'name' => 'required',
            'email' => 'required|email',
            
        ];
    }

    public function messages() {
       return [
           'name.required' => 'Name field is required',
           'email.required' => 'E-mail field is required'
       ];
    }
}