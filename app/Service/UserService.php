<?php 
namespace App\Service;

use App\Models\User;

class UserService{

    public function update(User $user, $name, $email){

        return $user->update(['name' => $name,'email'=> $email]);
        
    }
}