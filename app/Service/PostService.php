<?php

namespace App\Service;

use Illuminate\Support\Facades\Cache;
use App\Models\Post;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class PostService
{
    public function store($header,$content)
    {
      $post = new Post();
      $post->header = $header;
      $post->content = $content;
      $post->user_id = Auth()->user()->id;
      $result = $post->save();

      if($result){
          $posts = Cache::get('posts')->all();
          array_push($posts, $post);
          Cache::forever('posts', $posts);
      }

      return $post;
    }

    public function update(Post $post ,$request){
        $post->update(['header' => $request->header, 'content' => $request->content]);
    }
}
