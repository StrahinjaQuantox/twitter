<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Editing user') }}
    </h2>
</x-slot>
<form method="POST" action="{{ route("user.update", ['user' => $user->id]) }}">
  @method('PATCH')
  @csrf
  <label>Name</label> 
  <input type='text' name='name' style="display: block" @if(isset($user))value="{{ $user->name }}" @endif>
  <label>Email</label> 
  <input type='text' name='email'  style="display: block" @if(isset($user))value="{{ $user->email }}" @endif>
  <input type="submit" value="edit user">
</form>

@error('name')
{{ $message }}
@enderror
@error('email')
{{ $message }}
@enderror

@if(session('success'))
<p>Success!</p>
@endif
</x-app-layout>