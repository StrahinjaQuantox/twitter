<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Home page') }}
        </h2>
    </x-slot>

@auth
    <form action='logout' method='post'>
       @csrf
        <button type='submit' value=''>
            logout
        </button>
    </form>
       
    <a href=" {{route('post.create')}}">
        <button class='btn btn-primary'>
            What's on your mind lately?
        </button>
    </a>
       
    
    @else
    <a href='login'>login</a>
    <a href='register'>register</a>
    @endauth
    
    @php
        $i = 1;
    @endphp
    
    @foreach($posts as $post)
    <div class="container">
        <div class="row">
            <div class='col-2'>
                #{{ $i++ }}
            </div>
            <div class='col-10'>
                <h2>{{ $post->header }}</h2>
                <p>
                   
                    <span class="text-bold"> 
                        <a href='{{route('user.show', $post->user->id)}}'>
                            {{ $post->user->name}}: 
                        </a>
                    </span> 
               
                    {{$post->content}}
                    
                </p>
                <a href="{{route('post.edit',['post' => $post->id])}}">Edit</a>
            </div>
           
        </div>
    </div>
    <div>
        <a href=' {{ route("post.show",["post" => $post->id]) }}' target='_blank'>
            Open this post in a new window
        </a>
    </div>
    @endforeach
    
  </x-app-layout>