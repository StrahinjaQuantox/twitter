<x-app-layout>
    @if(session('success'))
<p style="color:green;">Success update</p>
    @endif
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Viewing user') }}
        </h2>
    </x-slot>
    
<p>Showing information about the user: </p><h3 style='display:inline-block'>Name: </h3>{{ ' '.$user->name . ''}} <a href="{{route('user.edit', $user->id)}}"><button>Edit this user</button></a>
<p><h3 style='display:inline-block'>E-mail: </h3>{{ ' '.$user->email . ' '}} </p>
Member since: {{$user->created_at}}
</x-app-layout>
