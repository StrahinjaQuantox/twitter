<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Viewing post') }}
        </h2>
    </x-slot>


<p>Showing information about the post: </p><h1>{{$post->header}}</h1>
<p>{{$post->content}}</p>
<p>Created at: {{$post->created_at}}</p>
    <p>Created by user: <a href="{{ route('user.show',['user' => $post->user->id])}}">{{$post->user->name}}</a></p>
    <p><a href="{{ route('post.edit', $post) }}">Edit this post</a></p>

</x-app-layout>
