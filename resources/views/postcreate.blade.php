<x-app-layout>
 <x-slot name="header">
  <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Creating posts') }}
  </h2>
</x-slot>
<form method="post" @if(isset($post))  Action="{{ route("post.update", ['post' => $post->id]) }}"  @else Action="{{ route("post.store") }}" @endif>
    @if(isset($post))
        @method('PATCH')

    @endif

      @csrf
    <label>Header</label>
    <input type='text' name='header' style="display: block" @if(isset($post))value="{{ $post->header }}" @endif>
    <label>Content</label>
    <input type='text' name='content'  style="display: block" @if(isset($post))value="{{ $post->content }}" @endif>
    <input type="submit" value="@if(isset($post)) Edit post @else Create post @endif">
</form>
@error('header')
{{ $message }}
@enderror
@error('content')
{{ $message }}
@enderror

@if(session('success'))
<p>Success!</p>
@endif
</x-app-layout>
